#!/bin/sh

xrandr --output DP-1 --off
sleep 2s
xrandr --output DP-1 --auto --right-of DP-2

nm-applet --indicator &
pa-applet &
picom &
lxsession