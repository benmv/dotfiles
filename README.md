# Configuration Files

Look, I trash my own system a lot trying new things. So here's the config files for the essentials (and a few of the things that aren't). It's only public so I can easily pull the files when I need, and as an incentive to make things look pretty.

Most of this is based on the defaults provided, so this is only useful for mild improvements for me. Qtile is where the main work is being done right now.

## Qtile

Dependencies:
- Picom (mainly for window shadows and opacity)
- Rofi (launcher)

The main window manager I use for my Arch setup. Nothing outrageous, aimed at X11 because I gave up on Wayland for a minute. Tried to call out dependencies in the comments (might have missed some).

## Others
Yeah there'll be more as I one-by-one put exceptions in the .gitignore file.

## arch_notes.md
I started making notes on steps to (re)install Arch Linux ages ago and have been adding to them since. Again, I want them dumped somewhere public so I can get to them in the event I brick things.